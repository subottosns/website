from decouple import config, Csv


post_url = config("SUBOTTO_FEEDER_WEB_URL", cast=str)
passwd_web = config("SUBOTTO_FEEDER_PWD", cast=str)

cur_year = config("SUBOTTO_FEEDER_YEAR", "2020", cast=int)
match_id = config("SUBOTTO_FEEDER_MATCH_ID", "1", cast=int)
old_matches_id = config("SUBOTTO_FEEDER_OLD_MATCHES_ID", "", cast=Csv(int))

# Sleep time between two feeder updates
sleep_time = config("SUBOTTO_FEEDER_REFRESH_TIME", 1.0, cast=float)
# Number of sleep cycles between two updates when no 24 ore is ongoing
idle_cycles_wait = config("SUBOTTO_FEEDER_IDLE_CYCLES_WAIT", 86400, cast=int)
