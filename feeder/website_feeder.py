#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import time
import requests, json
import traceback
import os

from libsubotto.listener import Statistics

import conf

os.environ['TZ'] = 'Europe/Rome'
time.tzset()

PASSWD = conf.passwd_web

def post_json_data(post_url, data):
    headers = {'content-type': 'application/json'}
    json_data = json.dumps(data)

    try:
        r = requests.post(post_url, data=json_data, headers=headers)
        print('Request done', r.status_code)
        return True
    except (requests.exceptions.InvalidSchema, requests.exceptions.ConnectionError):
        print("Website not available, continuing...", file=sys.stderr)
    except:
        print("Exception caught, continuing...", file=sys.stderr)
        traceback.print_exc()

def listen_match(match_id, old_matches_id, post_url):

    stats = Statistics(match_id, old_matches_id)

    matches_data = {"current": conf.cur_year, "old": dict([(m.year, m.id) for m in stats.old_matches]+[(conf.cur_year, match_id)])}
    matches_sent = None
    while matches_sent is None:
        print("Sending matches years and IDs")
        matches_sent = post_json_data(post_url, {'action': 'set', 'password': PASSWD, 'what': 'matches', 'data': matches_data})
        time.sleep(conf.sleep_time)

    try:
        # Post the score when booted up
        cycles_skipped = conf.idle_cycles_wait
        while True:
            if stats.core.is_match_ongoing() or cycles_skipped >= conf.idle_cycles_wait:
                stats.core.update()
                post_json_data(post_url, {'action': 'set', 'password': PASSWD, 'what': 'score', 'data': stats.data})
                cycles_skipped = 0
            else:
                cycles_skipped += 1
            time.sleep(conf.sleep_time)

    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    print(conf.match_id, conf.old_matches_id, conf.post_url)
    listen_match(conf.match_id, conf.old_matches_id, conf.post_url)
