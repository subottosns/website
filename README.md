# Servizi web

Il website_feeder parla con il database e POSTa i dati relativi alla 24ore corrente al server web.

## Come impostare le cose per una nuova 24ore

Feeder:
* In `settings.ini` set match_id and old_matches_id. Do not include
  match_id in old_matches_id.

Note: values in settings.ini are overwritten by env variables. This is
used, for instance, for the test feeder (`24_feeder_test.service` on soy)

Website: see README.md in website directory.
