from decouple import config

db_name = config("DB_NAME", "", cast=str)
db_user = config("DB_USER", "", cast=str)
db_password = config("DB_PASSWORD", "", cast=str)
db_port = config("DB_PORT", "", cast=str)
db_host = config("DB_HOST", "", cast=str)

db = "host={} port={} dbname={} user={} password={}".format(db_host, db_port, db_name, db_user, db_password) 

scorepw = config("SUBOTTO_WEB_PW", "superSecret", cast=str)
listen_addr = config("SUBOTTO_WEB_LISTEN_ADDR", "0.0.0.0", cast=str)
listen_port = config("SUBOTTO_WEB_LISTEN_PORT", "8080", cast=int)
