# Cosa fare per una nuova 24ore
* Aggiornare un po' di file statici:
  * `files/partials/navbar.html`: cambiare il numero dell'edizione e la data
  * `files/views/overview.html`: aggiungere il box per la nuova edizione
  * `files/views/schedule.html`: aggiungere i turni
  * `files/views/streaming.html`: aggiungere il nuovo link alla diretta
  * `files/images/logo.svg` (vedi sotto)
* Dopo l'inizio della 24 ore (senza fretta):
  * Aggiungere capitani, vice e prima coppia nella riga DB del match (servono solo nella pagina delle statistiche)
  * `files/views/trailers.html`
* Dopo le statistiche: aggiungere la riga con l'anno corrente in `files/js/navbar.js`

## Come aggiornare il logo
* Aprire il file con Inkscape `inkscape files/images/logo.svg`
* Fare doppio click sul testo fuori dal rettangolo che indica il confine del logo, e modificarlo
* Cancellare la versione presente all'interno del rettangolo, e sostituirla con una copia della nuova versione (il font è Porky's)
* Mettere come posizione (indicativamente) della nuova versione X=63, Y=1.8
* Importante: selezionare la nuova versione e dare il comando "Path > Object to Path"
* Salvare il file
